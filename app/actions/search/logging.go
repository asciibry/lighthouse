package search

import (
	"net/http"
	"time"

	"github.com/lbryio/lighthouse/app/util"

	"github.com/johntdyer/slackrus"

	"github.com/lbryio/lighthouse/app/env"
	"github.com/sirupsen/logrus"
)

var minTimeToDebugLog = 1 * time.Second
var minTimeToWarnLog = 1 * time.Minute
var searchLogger *logrus.Logger

// Init initializes search request logging
func Init(conf *env.Config) {
	searchLogger = logrus.New()
	searchLogger.SetLevel(logrus.DebugLevel)
	slackURL := conf.SlackHookURL
	slackChannel := conf.SlackChannel
	if slackURL != "" && slackChannel != "" {
		searchLogger.AddHook(&slackrus.SlackrusHook{
			HookURL:        slackURL,
			AcceptedLevels: slackrus.LevelThreshold(logrus.InfoLevel),
			Channel:        slackChannel,
			IconEmoji:      ":mag:",
			Username:       "search-inspector",
		})
	}
}

func (r searchRequest) log(req *http.Request, durToSearch time.Duration) {
	if durToSearch > minTimeToDebugLog {
		ip := util.GetIPAddressForRequest(req)
		agent := req.Header.Get("User-Agent")
		entry := logrus.NewEntry(searchLogger)
		entry = entry.WithField("type", r.searchType)
		entry = entry.WithField("ip", ip)
		entry = entry.WithField("agent", agent)
		entry = entry.WithField("duration", durToSearch.Seconds())
		entry = entry.WithField("search", r.S)
		if r.Debug {
			entry = entry.WithField("debug", true)
		}
		if r.Source {
			entry = entry.WithField("source", true)
		}
		if r.Resolve {
			entry = entry.WithField("resolve", true)
		}
		if r.ClaimID != nil {
			entry = entry.WithField("claim_id", *r.ClaimID)
		}
		if r.ChannelID != nil {
			entry = entry.WithField("channel_id", *r.ChannelID)
		}
		if r.Language != nil {
			entry = entry.WithField("language", *r.Language)
		}
		if r.TimeFilter != nil {
			entry = entry.WithField("time_filter", *r.TimeFilter)
		}
		if r.Include != nil {
			entry = entry.WithField("include", *r.Include)
		}
		if r.RelatedTo != nil {
			entry = entry.WithField("related_to", *r.RelatedTo)
		}
		if r.ContentType != nil {
			entry = entry.WithField("content_type", *r.ContentType)
		}
		if r.From != nil {
			entry = entry.WithField("from", *r.From)
		}
		if r.Size != nil {
			entry = entry.WithField("size", *r.Size)
		}
		if r.Filters != nil {
			entry = entry.WithField("filters", *r.Filters)
		}
		if r.FreeOnly != nil {
			entry = entry.WithField("free_only", *r.FreeOnly)
		}
		if r.MediaType != nil {
			entry = entry.WithField("media_type", *r.MediaType)
		}
		if r.SortBy != nil {
			entry = entry.WithField("sort_by", *r.SortBy)
		}
		if r.NSFW != nil {
			entry = entry.WithField("nsfw", *r.NSFW)
		}
		if durToSearch > minTimeToWarnLog {
			entry.Warn()
		} else {
			entry.Debug()
		}
	}
}
